import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AppUser } from '../model/AppUser';
import { Current } from '../model/Current';
import { CurrentService } from '../services/current-service/current.service';

@Component({
  selector: 'app-current',
  templateUrl: './current.component.html',
  styleUrls: ['./current.component.css']
})
export class CurrentComponent implements OnInit {

  //Create Form Group module
  createAccountForm!: FormGroup;

  statut = false;

  constructor(private currentService: CurrentService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    //Appel de la methode de creation du formulaire lors de l'initialisation de l'interface
    this.initCreateAccountForm();
  }

  //Methode d'initialisation du formulaire
  initCreateAccountForm() {
    this.createAccountForm = this.formBuilder.group({
      customerId: [''],
      initialCredit: ['']
    });
  }


  createAccount() {

    //Recuperation des valeurs du formulaire
    let customerId = this.createAccountForm.value.customerId;
    let initialCredit = this.createAccountForm.value.initialCredit;

    let appUser: AppUser = new AppUser();
    appUser.id = customerId;

    let current: Current = new Current();
    current.appuser = appUser
    current.initialCredit = initialCredit;

    //Save data
    this.currentService.createAccount(current).subscribe((response) => {
      this.statut = true;
    })
  }

}

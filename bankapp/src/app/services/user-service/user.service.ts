import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpClient: HttpClient) { }

  //CALL API METHOD
  /**
   * Create Account API Call Method
   */

  getUsers(): Observable<any> {
    return this.httpClient.get(environment.host + "appuser");
  }

  getUser(id: any): Observable<any> {
    
    return this.httpClient.get(environment.host + "appuser/find/"+id);
  }
}

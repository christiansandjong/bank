import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Current } from 'src/app/model/Current';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CurrentService {

  constructor(private httpClient: HttpClient) { }

  //CALL API METHOD
  /**
   * Create Account API Call Method
   */

   createAccount(current: Current): Observable<any> {
    return this.httpClient.post(environment.host + "current/add", current);
  }
}

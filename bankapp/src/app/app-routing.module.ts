import { CurrentComponent } from './current/current.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserComponent } from './user/user.component';

const routes: Routes = [
  { path: 'bank/current', component: CurrentComponent },
  { path: '', component: CurrentComponent },
  { path: 'bank', component: CurrentComponent },
  { path: 'bank/user', component: UserComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { AppUser } from "./AppUser";

export class Current {
    id?: number;
    initialCredit?: number;

    appuser?: AppUser;
}
export class AppUser{
	id?: number;
    name?: string;
	surname?: string;
	balance?: number;
}
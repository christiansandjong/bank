import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { UserService } from '../services/user-service/user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  //Create Form Group module
  userDetailForm!: FormGroup;

  //User response
  user?: any;

  constructor(private userService: UserService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.initForm()
  }

  //Methode d'initialisation du formulaire
  initForm() {
    this.userDetailForm = this.formBuilder.group({
      customerId: ['']
    });
  }


  getUserData() {
    let customerId = this.userDetailForm.value.customerId;

    this.userService.getUser(customerId).subscribe({
      next: (data) => {
        console.log(data);
        this.user = data;
        
      },
      error: (error) => {
        console.log(error);

      }
    })
  }

}

package be.smc.bank.repo;

import be.smc.bank.model.Current;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CurrentRepo extends JpaRepository<Current, Long> {

}

package be.smc.bank.repo;

import be.smc.bank.model.Appuser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AppuserRepo extends JpaRepository<Appuser, Long> {

    Optional<Appuser> findAppUserById(Long aLong);

    void deleteAppUserById(Long id);
}

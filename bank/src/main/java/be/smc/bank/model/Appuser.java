package be.smc.bank.model;

import com.sun.istack.NotNull;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table
@Getter
@Setter
@NoArgsConstructor
public class Appuser implements Serializable{

	private static final long
			serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name ="custumer_id" , nullable = false, updatable = false)
	protected Long id;

	@Column(nullable = false)
	private String name;
	@NotNull
	private String surname;
	@NotNull
	private Double balance;

	@OneToMany (mappedBy = "appuser")
	Set<Current> current = new HashSet<>();





}

package be.smc.bank.model.dto;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class AppuserDTO  {

	protected Long id;
	private String name;
	private String surname;
	private Double balance;
	List<CurrentDTO> currentDTO;

}

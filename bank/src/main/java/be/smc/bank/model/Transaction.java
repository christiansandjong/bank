package be.smc.bank.model;

import com.sun.istack.NotNull;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table
@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
public class Transaction implements Serializable{

	private static final long
			serialVersionUID = 1L;


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name ="transaction_id" , nullable = false, updatable = false)
	protected Long id;

	@Column(nullable = false)
	private String type;

	@NotNull
	@ManyToOne
	@JoinColumn(name ="current_id" ,nullable = false, updatable = false)
	private Current current;

}

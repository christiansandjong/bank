package be.smc.bank.model.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class TransactionDTO {

	protected Long id;
	private String type;
	private CurrentDTO currentDTO;

}

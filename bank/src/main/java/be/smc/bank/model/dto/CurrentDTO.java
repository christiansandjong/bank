package be.smc.bank.model.dto;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;


@Getter
@Setter
@NoArgsConstructor
public class CurrentDTO {

    protected Long id;
    private Double initialCredit;
    private AppuserDTO appuserDTO;
    List<TransactionDTO> transactionDTO;

}

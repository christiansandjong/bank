package be.smc.bank.model;


import com.sun.istack.NotNull;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table
@Getter
@Setter
@NoArgsConstructor
public class Current implements Serializable {

    private static final long
            serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name ="current_id" , nullable = false, updatable = false)
    protected Long id;

    @Column
    private Double initialCredit;

    @NotNull
    @ManyToOne
    @JoinColumn(name ="custumer_id" ,nullable = false, updatable = false)
    private Appuser appuser;

    @OneToMany (mappedBy = "current")
    Set<Transaction> transaction = new HashSet<>();

}

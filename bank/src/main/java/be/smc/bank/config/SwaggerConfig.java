package be.smc.bank.config;

import org.springframework.context.annotation.Configuration;

/**
 * Swagger est un langage de description d'interface permettant de décrire
 * des API RESTful exprimées à l'aide de JSON. Swagger est utilisé avec toute
 * une
 * série d'outils logiciels open source pour concevoir, créer, documenter et
 * utiliser
 * des services Web RESTful.
 * Fichier de Configuration de Swagger
 */
@Configuration
public class SwaggerConfig {

    

}

package be.smc.bank.controller;

import be.smc.bank.exception.UserNotFoundException;
import be.smc.bank.model.Appuser;
import be.smc.bank.model.Current;
import be.smc.bank.model.dto.AppuserDTO;
import be.smc.bank.model.dto.CurrentDTO;
import be.smc.bank.model.dto.TransactionDTO;
import be.smc.bank.service.interfaces.AppuserService;
import be.smc.bank.service.interfaces.CurrentService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1")
@Tag(name = "/bank")
public class BankControllerRest {

    @Autowired
    private final CurrentService currentService;
    @Autowired
    private final AppuserService appuserService;
    @Autowired
    private ModelMapper modelMapper;

    public BankControllerRest(CurrentService currentService, AppuserService appuserService) {
        this.currentService = currentService;
        this.appuserService = appuserService;
    }

    // Appuser
    // GET : http://localhost:8080/bank/api/v1/appuser/
    @GetMapping("/appuser")
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Get all users.", description = "This method is used to get all categories.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful users get."),
            @ApiResponse(responseCode = "404", description = "Not Found."),
    })
    public ResponseEntity<List<AppuserDTO>> getAllAppusers() {
        List<Appuser> appusers = appuserService.findAllAppUsers();
        // chaque appuser de la liste appusers ser le parametre de convertAppuser
        List<AppuserDTO> appuserDTO = appusers.stream().map( this::convertAppuserDTO).collect(Collectors.toList());
        return new ResponseEntity<>(appuserDTO, HttpStatus.OK);
    }

    // GET : http://localhost:8080/bank/api/v1/appuser/find/{id}
    // @ResponseStatus(HttpStatus.OK)
    @GetMapping("/appuser/find/{id}")
    public ResponseEntity<AppuserDTO> getAppuserById(@PathVariable("id") Long id) {
        Appuser appuser = appuserService.findAppUserById(id);
        AppuserDTO appuserDTO = convertAppuserDTO(appuser);
        return new ResponseEntity<>(appuserDTO, HttpStatus.OK);
    }

    // Current
    // POST : http://localhost:8080/bank/api/v1/current/add
    // @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/current/add")
    public ResponseEntity<?> addCurrent(@RequestBody Current current) throws Exception {

        Current current2 = null;
        try {
            current2 = currentService.addCurrent(current);
        } catch (UserNotFoundException e) {
            throw e;
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    AppuserDTO convertAppuserDTO(Appuser appuser) {

        // map a list
        List<CurrentDTO> currentDTO = appuser.getCurrent().stream().map(this::convertCurrentDTO).collect(Collectors.toList());
        // map a object
        //CurrentDTO currentDTO = modelMapper.map(appuser.getCurrent(), CurrentDTO.class);
        AppuserDTO appuserDTO = modelMapper.map(appuser, AppuserDTO.class);
        appuserDTO.setCurrentDTO(currentDTO);
        return appuserDTO;
    }

    CurrentDTO convertCurrentDTO(Current current) {
        // map a list
        List<TransactionDTO> transactionDTO = current.getTransaction().stream().map(
                c -> modelMapper.map(c, TransactionDTO.class)).collect(Collectors.toList());
        CurrentDTO currentDTO = modelMapper.map(current, CurrentDTO.class);
        currentDTO.setTransactionDTO(transactionDTO);
        return currentDTO;
    }


}

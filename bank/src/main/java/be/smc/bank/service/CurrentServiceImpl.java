package be.smc.bank.service;

import be.smc.bank.exception.UserNotFoundException;
import be.smc.bank.model.Appuser;
import be.smc.bank.model.Current;
import be.smc.bank.model.Transaction;
import be.smc.bank.repo.AppuserRepo;
import be.smc.bank.repo.CurrentRepo;
import be.smc.bank.repo.TransactionRepo;
import be.smc.bank.service.interfaces.CurrentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CurrentServiceImpl implements CurrentService {
    private static final String DEFAULT_TYPE = "RETRAIT";
    private final CurrentRepo currentRepo;
    private final TransactionRepo transactionRepo;
    private final AppuserRepo appuserRepo;

    @Autowired
    public CurrentServiceImpl(CurrentRepo currentRepo, TransactionRepo transactionRepo, AppuserRepo appuserRepo) {
        this.currentRepo = currentRepo;
        this.transactionRepo = transactionRepo;
        this.appuserRepo = appuserRepo;
    }


    @Override
    public Current addCurrent(Current current)  {
        if (current.getInitialCredit() != 0) {
            Appuser appuser = appuserRepo.findAppUserById(current.getAppuser()
                    .getId()).orElseThrow(() -> new UserNotFoundException(
                    "appuser by id " + current.getAppuser()
                            .getId() + " was not found"));
            double newBalance = appuser.getBalance() - current.getInitialCredit();
            appuser.setBalance(newBalance);
            appuserRepo.save(appuser);
            current.setAppuser(appuser);
            Current newCurrent = currentRepo.save(current);
            Transaction transaction = new Transaction();
            transaction.setCurrent(newCurrent);
            transaction.setType(DEFAULT_TYPE);
            transactionRepo.save(transaction);
            return newCurrent;
        }
        return currentRepo.save(current);
    }

}

package be.smc.bank.service.interfaces;

import be.smc.bank.model.Current;

public interface CurrentService {

    Current addCurrent(Current current) throws Exception;

}




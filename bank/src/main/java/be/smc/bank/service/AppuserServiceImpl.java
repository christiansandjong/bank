package be.smc.bank.service;

import be.smc.bank.exception.UserNotFoundException;
import be.smc.bank.model.Appuser;
import be.smc.bank.repo.AppuserRepo;
import be.smc.bank.service.interfaces.AppuserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AppuserServiceImpl implements AppuserService {
    private final AppuserRepo appuserRepo;

    @Autowired
    public AppuserServiceImpl(AppuserRepo appuserRepo) {
        this.appuserRepo = appuserRepo;
    }

    public Appuser findAppUserById(Long id) {
        return Optional.ofNullable(appuserRepo.findAppUserById(id)
                .orElseThrow(() -> new UserNotFoundException(
                        "appuser by id " + id + " was not found"))).get();
    }

    public List<Appuser> findAllAppUsers() {
        return appuserRepo.findAll();
    }

    public Appuser updateAppUser(Appuser appuser) {
        return appuserRepo.save(appuser);
    }

    public void deleteUser(Long id) {
        appuserRepo.deleteAppUserById(id);
    }
}

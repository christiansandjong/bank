package be.smc.bank.service.interfaces;

import be.smc.bank.model.Appuser;

import java.util.List;

public interface AppuserService {

    Appuser findAppUserById(Long id);
    List<Appuser> findAllAppUsers();

}




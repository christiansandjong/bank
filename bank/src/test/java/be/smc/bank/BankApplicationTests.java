
package be.smc.bank;

import be.smc.bank.model.Appuser;
import be.smc.bank.model.Current;
import be.smc.bank.service.interfaces.AppuserService;
import be.smc.bank.service.interfaces.CurrentService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
class BankApplicationTests {

    @Autowired
    CurrentService currentService;
    @Autowired
    AppuserService appuserService;

    @Test
    void contextLoads() {
    }

    @Test
    public void testCreateCurrentAccount() throws Exception {
        Appuser appuser = appuserService.findAppUserById(1L);
        Current current = new Current();
        current.setAppuser(appuser);
        current.setInitialCredit(50.0);
        currentService.addCurrent(current);
        assertEquals(current.getId(), 1L);
    }

    @Test
    public void testReadAppuserInfos() {
        Appuser appuser = appuserService.findAppUserById(1L);
        assertNotNull(appuser);
        assertEquals("chris", appuser.getSurname());
        assertEquals("chris", appuser.getSurname());
        assertEquals("chris", appuser.getSurname());
        assertEquals("chris", appuser.getSurname());

    }
}


CAPGEMINI 

-- 1- run this script to create database
drop database if exists bank;
create database bank;
use bank;

-- 2- run the java app SpringBoot to create Tables
-- 3- run this script to insert samples data
insert into appuser(custumer_id, balance, name, surname)
values (1, 2000, 'Capgemini', 'Dev'),
       (2, 5000, 'Cpagemini', 'Prod');

-- 4- run the fornt End on vscode or other  ( ng serve) angular
-- 5- open this link on your browser http://localhost:4200/
-- 6- test the app (make sure the java back-end is still running) 


